package com.techprimers.jpa.springjpahibernateexample.respository;

import com.techprimers.jpa.springjpahibernateexample.model.UserContact;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsersContactRepository extends JpaRepository<UserContact, Integer> {



}
