package com.techprimers.jpa.springjpahibernateexample.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@Configuration
public class JpaConfig {
}
